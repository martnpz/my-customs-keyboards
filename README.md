# My handwired custom keyboards.
## _mk_ergo_ & _mk_40_

Hand-wired keyborad collection powered by [QMK firmware.](https://github.com/qmk/qmk_firmware)

# mk_ergo
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_ergo/images/layout.png)
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_ergo/images/promicro_left.png)
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_ergo/images/promicro_right.png)
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_ergo/images/promicro_wire_position.png)

# mk_40
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_40/images/promicro.png)
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_40/images/layout.png)
![ ](https://gitlab.com/martnpz/my-customs-keyboards/-/raw/main/mk_40/images/wire_position.png)

# Linux set-up
## Custom [eurkey](https://eurkey.steffen.bruentjen.eu/) layout
+ download the layout from https://gitlab.com/martnpz/dots/-/tree/main/kb_layout
```sh
sudo cp eu /usr/share/X11/xkb/symbols/eu
setxkbmap eu   
```

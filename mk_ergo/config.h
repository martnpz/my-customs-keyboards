#pragma once

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE 5

#define USE_SERIAL
#define MASTER_LEFT
//#define EE_HANDS
//#define SPLIT_USB_DETECT

#define MOUSEKEY_TIME_TO_MAX 20
#define MOUSEKEY_MAX_SPEED 12
#define MOUSEKEY_MOVE_DELTA 6

#ifdef AUDIO_ENABLE
    #define AUDIO_PIN B6
    #define STARTUP_SONG SONG(ROCK_A_BYE_BABY)
    #define GOODBY_SONG
    #define AUDIO_CLICKY_FREQ_DEFUALT 1500.0f
    // At ~/qmk_firmware/quantum/process_keycode/process_clicky.c
    // Modify clicky_song[2][0] = clicky_freq * (8.0f...
    #define AUDIO_CLICKY
#endif



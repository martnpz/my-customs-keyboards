/*
Copyright 2021 @martnpz
Copyright 2022 @martnpz
Copyright 2024 @martnpz
*/

#include QMK_KEYBOARD_H
#include "quantum/keymap_extras/keymap_spanish.h"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Workman
     * |-----------------------------------------------------.                      .----------------------------------------------------|
     * |KC_ESC,     KC_Q,    KC_D,    KC_R,    KC_W,    KC_B,|                      | KC_J,    KC_F,    KC_U,    KC_P, KC_LBRC, KC_ACUT  |
     * |--------+--------+--------+--------+--------+--------|                      |--------+--------+--------+--------+--------+-------|
     * |KC_TAB,     KC_A,    KC_S,    KC_H,    KC_T,    KC_G,|                      | KC_Y,    KC_N,    KC_E,    KC_O,    KC_I, KC_RBRC  |
     * |--------+--------+--------+--------+--------+--------|                      |--------+--------+--------+--------+--------+-------|
     * |KC_LSFT,    KC_Z,    KC_X,    KC_M,    KC_C,    KC_V,                         KC_K,    KC_L, KC_COMM,  KC_DOT, KC_MINS, KC_EQL  |
     * |--------+--------+--------+--------+--------+--------+---------|  |---------+--------+--------+--------+--------+--------+-------|
     *                       KC_LCTL, KC_LGUI, MO(1), KC_SPC, KC_LALT  |  |  KC_BSPC,  KC_ENT,  KC_MO(2), KC_RALT, K_SLSH
     *                                       |-------------------------|  |--------------------------|
 */

	LAYOUT_mk_ergo(
		KC_ESC,     KC_Q,    KC_D,    KC_R,    KC_W,    KC_B,                         KC_J,    KC_F,    KC_U,    KC_P, KC_LBRC, ES_ACUT,
		KC_TAB,     KC_A,    KC_S,    KC_H,    KC_T,    KC_G,                         KC_Y,    KC_N,    KC_E,    KC_O,    KC_I, KC_RBRC,
		KC_LSFT,    KC_Z,    KC_X,    KC_M,    KC_C,    KC_V,                         KC_K,    KC_L, KC_COMM,  KC_DOT, KC_MINS, KC_EQL,
		                       KC_LCTL, KC_LGUI, MO(1), KC_SPC, KC_LALT,     KC_BSPC, KC_ENT, MO(2),   KC_RALT, KC_SLSH),

	LAYOUT_mk_ergo(
		KC_ESC,     KC_MPLY, KC_1,    KC_2,    KC_3,    KC_VOLU,                      KC_F1,   KC_F2,   KC_F3,  KC_F4, KC_BSLS, KC_PIPE,
		KC_CAPS,    KC_MNXT, KC_4,    KC_5,    KC_6,    KC_VOLD,                      KC_F5,   KC_F6,   KC_F7,  KC_F8, KC_COLN, KC_SCLN,
		_______,    KC_MPRV, KC_7,    KC_8,    KC_9,    KC_0,                         KC_F9,   KC_F10, KC_F11, KC_F12, _______, KC_EQL,
		                    _______, _______, _______, _______, KC_MUTE,     _______, _______, _______, _______, KC_TILD),

	LAYOUT_mk_ergo(
		_______,    _______, KC_BTN1, KC_MS_U, KC_BTN2, _______,                    _______, KC_HOME,   KC_UP, KC_PGUP, _______,_______,
		_______,    _______, KC_MS_L, KC_MS_D, KC_MS_R, _______,                    _______, KC_LEFT, KC_DOWN, KC_RGHT, _______,_______,
		_______,    _______, KC_WH_D, _______, KC_WH_U, _______,                    _______,  KC_END, _______,KC_PGDN, _______, _______,
		                     _______,_______, _______, _______, QK_BOOT,     KC_DEL, CK_TOGG, _______, _______, _______),
};

